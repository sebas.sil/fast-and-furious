package br.com.corunda.fastandfurious;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class SensorsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensors);

        ScrollView scroll = this.findViewById(R.id.sensor_scroll_view);
        LinearLayout array = (LinearLayout) scroll.getChildAt(0);

        // item para adicionar a lista o sensor de GPS
        View gps = LayoutInflater.from(getBaseContext()).inflate(R.layout.sensor_item, null, false);
        ((TextView) gps.findViewById(R.id.sensor_name)).setText("GPS");
        ((TextView) gps.findViewById(R.id.sensor_description)).setText("Responsável por capturar a posição do aparelho (x,y) para saber sua localização em tempo real");
        ((ImageView) gps.findViewById(R.id.sensor_icon)).setImageResource(R.drawable.ic_gps_fixed_black);
        array.addView(gps);

        // item para adicionar a lista o sensor de acelerometro
        View accelerometer = LayoutInflater.from(getBaseContext()).inflate(R.layout.sensor_item, null, false);
        ((TextView) accelerometer.findViewById(R.id.sensor_name)).setText("Acelerometro");
        ((TextView) accelerometer.findViewById(R.id.sensor_description)).setText("Responsável por capturar as forças que agem sobre o aparelho e saber se esta acelerando ou freiando");
        ((ImageView) accelerometer.findViewById(R.id.sensor_icon)).setImageResource(R.drawable.ic_accelerometer_black);
        array.addView(accelerometer);

        // item para adicionar a lista o sensor de bluethooth
        View bluetooth = LayoutInflater.from(getBaseContext()).inflate(R.layout.sensor_item, null, false);
        ((TextView) bluetooth.findViewById(R.id.sensor_name)).setText("Bluetooth");
        ((TextView) bluetooth.findViewById(R.id.sensor_description)).setText("Responsável pela comunicação com sensores externos");
        ((ImageView) bluetooth.findViewById(R.id.sensor_icon)).setImageResource(R.drawable.ic_bluetooth_black);
        array.addView(bluetooth);

        // verifica o status do sensor (se tem ou nao)
        SensorManager sensorManager = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
        if (sensorManager != null) {

            boolean permissionLocationApproved = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (locationManager != null) {
                ((TextView) gps.findViewById(R.id.sensor_status)).setText("existe");
                if(permissionLocationApproved){
                    ((TextView) gps.findViewById(R.id.sensor_status)).setText(((TextView) gps.findViewById(R.id.sensor_status)).getText() + " / permitido");
                } else {
                    ((TextView) gps.findViewById(R.id.sensor_status)).setText(((TextView) gps.findViewById(R.id.sensor_status)).getText() + " / NAO permitido");
                }
            } else {
                ((TextView) gps.findViewById(R.id.sensor_status)).setText("não existe");
            }

            Sensor accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
            if (accelerometerSensor != null) {
                ((TextView) accelerometer.findViewById(R.id.sensor_status)).setText("existe");
            } else {
                ((TextView) accelerometer.findViewById(R.id.sensor_status)).setText("não existe");
            }

            BluetoothAdapter bluetoothSensor = BluetoothAdapter.getDefaultAdapter();
            if (bluetoothSensor != null) {
                ((TextView) bluetooth.findViewById(R.id.sensor_status)).setText("existe");
            } else {
                ((TextView) bluetooth.findViewById(R.id.sensor_status)).setText("não existe");
            }
        }

    }
}