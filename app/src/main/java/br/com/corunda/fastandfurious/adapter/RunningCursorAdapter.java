package br.com.corunda.fastandfurious.adapter;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DecimalFormat;

import br.com.corunda.fastandfurious.EditRunningActivity;
import br.com.corunda.fastandfurious.MapsActivity;
import br.com.corunda.fastandfurious.R;
import br.com.corunda.fastandfurious.RunningActivity;
import br.com.corunda.fastandfurious.database.FastContract;
import br.com.corunda.fastandfurious.database.FuriousProvider;

public class RunningCursorAdapter extends RecyclerView.Adapter<RunningCursorAdapter.VH> {

    public static final String TAG = "RunningCursorAdapter";
    private Cursor cursor;
    private DecimalFormat df = new DecimalFormat("0.0");

    public RunningCursorAdapter(Cursor cursor) {
//        Log.v(TAG, "RunningCursorAdapter");
        this.cursor = cursor;

        Log.v(TAG, "items count: " + getItemCount());
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        Log.v(TAG, "onCreateViewHolder");
        final Context context = parent.getContext();
        View v = LayoutInflater.from(context).inflate(R.layout.running_item, parent, false);

        final VH vh = new VH(v);

        TextView t = v.findViewById(R.id.running_name);
        ImageView d = v.findViewById(R.id.running_delete);
        ImageView e = v.findViewById(R.id.running_edit);

        t.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MapsActivity.class);

                int position = vh.getAdapterPosition();
                cursor.moveToPosition(position);

                long id = cursor.getLong(cursor.getColumnIndex(FastContract.RunningEntry._ID));

                intent.putExtra("running", id);
                context.startActivity(intent);
            }
        });

        d.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                int position = vh.getAdapterPosition();
                cursor.moveToPosition(position);

                long id = cursor.getLong(cursor.getColumnIndex(FastContract.RunningEntry._ID));
                Uri singleUri = ContentUris.withAppendedId(FuriousProvider.CONTENT_RUNNING_URI,id);
                int qtd = context.getContentResolver().delete(singleUri, null, null);
                boolean deleted = qtd == 1;
                if(deleted) {
                    context.getContentResolver().notifyChange(FuriousProvider.CONTENT_RUNNING_URI, null);
                }
                Log.v(TAG, "Deleted item: " + (deleted));
            }
        });

        e.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EditRunningActivity.class);

                int position = vh.getAdapterPosition();
                cursor.moveToPosition(position);

                long id = cursor.getLong(cursor.getColumnIndex(FastContract.RunningEntry._ID));

                intent.putExtra("running", id);
                ((Activity)context).startActivityForResult(intent, RunningActivity.EDIT_CONTACT_REQUEST);

            }
        });

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
//        Log.v(TAG, "onBindViewHolder, " + position);
        cursor.moveToPosition(position);
        int idxName = cursor.getColumnIndex(FastContract.RunningEntry.COLUMN_NAME_NAME);
        int idxDistance = cursor.getColumnIndex("distance");
        int idxTime = cursor.getColumnIndex("time");
        int idxVelocity = cursor.getColumnIndex("velocity");
        int idxAltitude = cursor.getColumnIndex("altitude");

        String location = cursor.getString(idxName);
        float distance = cursor.getFloat(idxDistance) / 1000;
        int time = (int)(cursor.getFloat(idxTime) / 1000 / 60); // tempo em minutos
        float velocity = cursor.getFloat(idxVelocity);
        float altitude = cursor.getFloat(idxAltitude);

        holder.location.setText(location);
        holder.setValue(holder.distance, df.format(distance), "km");
        holder.setValue(holder.time, String.valueOf(time), "min");
        holder.setValue(holder.velocity, df.format(velocity), "km/h");
        holder.setValue(holder.altitude, df.format(altitude), "m");


    }

    @Override
    public long getItemId(int position) {
//        Log.v(TAG, "getItemId, " + position);
        int id = 0;
        if (cursor != null && cursor.moveToPosition(position)) {
            int idx_id = cursor.getColumnIndex(FastContract.RunningEntry._ID);
            return cursor.getLong(idx_id);
        }
        return id;
    }

    @Override
    public int getItemCount() {
//        Log.v(TAG, "getItemCount");
        int ret = 0;
        if (cursor != null) {
            cursor.moveToLast();
            ret = cursor.getPosition() + 1;
        }
        return ret;
    }

    @Override
    public void registerAdapterDataObserver(@NonNull RecyclerView.AdapterDataObserver observer) {
        super.registerAdapterDataObserver(observer);
        Log.v(TAG, "registerAdapterDataObserver: " + observer);
    }

    public static class VH extends RecyclerView.ViewHolder {
        public TextView location;
        public View distance;
        public View time;
        public View velocity;
        public View altitude;


        public VH(View v) {
            super(v);
            location = v.findViewById(R.id.running_name);
            distance = v.findViewById(R.id.running_sub_distance);
            time     = v.findViewById(R.id.running_sub_time);
            velocity = v.findViewById(R.id.running_sub_velocity);
            altitude = v.findViewById(R.id.running_sub_altitude);

            location.setTooltipText(v.getContext().getString(R.string.running_txt_sub_item_name_tooltip));
            distance.setTooltipText(v.getContext().getString(R.string.running_txt_sub_item_distance_tooltip));
            time.setTooltipText(v.getContext().getString(R.string.running_txt_sub_item_time_tooltip));
            velocity.setTooltipText(v.getContext().getString(R.string.running_txt_sub_item_velocity_tooltip));
            altitude.setTooltipText(v.getContext().getString(R.string.running_txt_sub_item_altitude_tooltip));
        }

        void setValue(View item, String value, String unity){
            TextView v = item.findViewById(R.id.sub_item_text);
            v.setText(value);

            TextView u = item.findViewById(R.id.sub_item_unity);
            u.setText(unity);
        }

    }


}
