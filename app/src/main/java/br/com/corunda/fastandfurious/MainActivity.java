package br.com.corunda.fastandfurious;

import android.Manifest;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.text.DecimalFormat;
import java.util.Arrays;

import br.com.corunda.fastandfurious.service.FuriousService;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";
    public static final String REQUEST_LOCATION_UPDATE = "location_update";
    public static final String REQUEST_ACCELEROMETER_UPDATE = "accelerometer_update";
    private static final int REQUEST_CHECK_SETTINGS = 1;
    private TextView velocityTextView = null;
    private TextView accelerometerTextView = null;


    private BroadcastReceiver receiver = new BroadcastReceiver() {

        private DecimalFormat df_0 = new DecimalFormat("0");

        @Override
        public void onReceive(Context context, Intent intent) {
            //Log.v(TAG, "onReceive: " + intent);
            Bundle extras = intent.getExtras();
            if (extras != null) {
                switch (intent.getAction()) {
                    case REQUEST_LOCATION_UPDATE:
                        velocityTextView.setText(df_0.format(((Location) extras.get("location")).getSpeed() * 3.6));
                        break;
                    case REQUEST_ACCELEROMETER_UPDATE:
                        float[] values = extras.getFloatArray("accelerometer_values");
                        accelerometerTextView.setText(df_0.format(Math.sqrt(Math.pow(values[0], 2) + Math.pow(values[1], 2) + Math.pow(values[2], 2))));
                        break;
                }
            }
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            // Checking whether user granted the permission or not.
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                final Button updates = findViewById(R.id.btn_request_location);
                updates.setText(R.string.request_location_updates_start);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button updates = findViewById(R.id.btn_request_location);
        final Button maps = findViewById(R.id.btn_view_location_updates);
        final Button sensors = findViewById(R.id.btn_view_sensors);

        velocityTextView = findViewById(R.id.mark_velocity);
        accelerometerTextView = findViewById(R.id.mark_acceleration);

        boolean permissionAccessCoarseLocationApproved = ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        if (!permissionAccessCoarseLocationApproved) {
            updates.setText(R.string.request_location_updates);
        }

        updates.setOnClickListener(new View.OnClickListener() {
            boolean isRunning = isMyServiceRunning(FuriousService.class);

            @Override
            public void onClick(View view) {
                boolean permissionAccessCoarseLocationApproved = ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
                if (!permissionAccessCoarseLocationApproved) {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CHECK_SETTINGS);
                } else {
                    if (isRunning) {
                        isRunning = false;
                        stop();
                    } else {
                        isRunning = true;
                        start();
                    }
                    changeUI(isRunning);
                }
            }
        });


        maps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, RunningActivity.class);
                //intent.putExtra("listener", listener);
                startActivity(intent);
            }
        });

        sensors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SensorsActivity.class);
                startActivity(intent);
            }
        });
    }


    @Override
    protected void onDestroy() {
        this.stop();
        super.onDestroy();
    }

    private void start() {
        startService(new Intent(MainActivity.this, FuriousService.class));
    }

    private void changeUI(boolean isRunning){
        final View v = findViewById(R.id.marker_layout);
        final Button updates = findViewById(R.id.btn_request_location);
        if(isRunning) {
            updates.setText(R.string.request_location_updates_stop);
            v.setVisibility(View.VISIBLE);
        } else {
            updates.setText(R.string.request_location_updates_start);
            v.setVisibility(View.INVISIBLE);
        }
    }


    private void stop() {
        stopService(new Intent(MainActivity.this, FuriousService.class));
    }

    @Override
    protected void onPause() {
        Log.v(TAG, "onPause");
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    @Override
    protected void onResume() {
        Log.v(TAG, "onResume");
        boolean isRunning = isMyServiceRunning(FuriousService.class);
        Log.v(TAG, "is service running? " + isRunning);
        changeUI(isRunning);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(MainActivity.REQUEST_LOCATION_UPDATE));
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(MainActivity.REQUEST_ACCELEROMETER_UPDATE));
        super.onResume();
    }

    @Override
    protected void onStart() {
        Log.v(TAG, "onStart");
        super.onStart();
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}
