package br.com.corunda.fastandfurious.listener;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import br.com.corunda.fastandfurious.MainActivity;

public class AccelerometerListener implements SensorEventListener {

    public static final String TAG = "AccelerometerListener";
    private Intent intent = new Intent(MainActivity.REQUEST_ACCELEROMETER_UPDATE);

    private SensorEvent sensor = null;
    private Context context = null;

    public AccelerometerListener(Context context){
        this.context = context;
    }

    @Override
    public void onSensorChanged(SensorEvent sensor) {
        //Log.v(TAG, "onSensorChanged");
        this.sensor = sensor;
        // send value to the receiver
        intent.putExtra("accelerometer_values", sensor.values);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public SensorEvent getSensor(){
        return sensor;
    }

}
