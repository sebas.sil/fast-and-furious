package br.com.corunda.fastandfurious.database;

import android.provider.BaseColumns;

public final class FastContract {
    static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + PointEntry.TABLE_NAME + " (" +
                    PointEntry._ID + " INTEGER PRIMARY KEY," +
                    PointEntry.COLUMN_NAME_RUNNING  + " INTEGER," +
                    PointEntry.COLUMN_NAME_LATITUDE + " REAL," +
                    PointEntry.COLUMN_NAME_LONGITUDE + " REAL," +
                    PointEntry.COLUMN_NAME_ALTITUDE + " REAL," +
                    PointEntry.COLUMN_NAME_ACCURACY + " REAL," +
                    PointEntry.COLUMN_NAME_SPEED + " REAL," +
                    PointEntry.COLUMN_NAME_SPEED_ACCURACY + " REAL," +
                    PointEntry.COLUMN_NAME_TIME + " INTEGER," +
                    PointEntry.COLUMN_NAME_DISTANCE + " REAL," +
                    PointEntry.COLUMN_NAME_ACCELERATION_X + " REAL, " +
                    PointEntry.COLUMN_NAME_ACCELERATION_Y + " REAL, " +
                    PointEntry.COLUMN_NAME_ACCELERATION_Z + " REAL)";

    static final String SQL_CREATE_RUNNING =
            "CREATE TABLE " + RunningEntry.TABLE_NAME + " (" +
                    RunningEntry._ID + " INTEGER PRIMARY KEY," +
                    RunningEntry.COLUMN_NAME_NAME + " TEXT)";

    static final String SQL_DELETE_ENTRIES  = "DROP TABLE IF EXISTS " + PointEntry.TABLE_NAME;
    static final String SQL_DELETE_RUNNING = "DROP TABLE IF EXISTS " + RunningEntry.TABLE_NAME;

    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private FastContract() {
    }

    /* Inner class that defines the table contents */
    public static class PointEntry implements BaseColumns {
        public static final String TABLE_NAME = "FF";
        public static final String COLUMN_NAME_RUNNING = "running_id";
        public static final String COLUMN_NAME_LATITUDE = "latitude";
        public static final String COLUMN_NAME_LONGITUDE = "longitude";
        public static final String COLUMN_NAME_ALTITUDE = "altitude";
        public static final String COLUMN_NAME_ACCURACY = "accuracy";
        public static final String COLUMN_NAME_SPEED = "speed";
        public static final String COLUMN_NAME_SPEED_ACCURACY = "speed_accuracy";
        public static final String COLUMN_NAME_TIME = "time";
        public static final String COLUMN_NAME_DISTANCE = "distance";
        public static final String COLUMN_NAME_ACCELERATION_X = "accelerationX"; // para tras e para direita e esquerda
        public static final String COLUMN_NAME_ACCELERATION_Y = "accelerationY"; // para tras e para cima e baixo
        public static final String COLUMN_NAME_ACCELERATION_Z = "accelerationZ"; // para tras e para frente na tela
    }

    /* Inner class that defines the table contents */
    public static class RunningEntry implements BaseColumns {
        public static final String TABLE_NAME = "RN";
        public static final String COLUMN_NAME_NAME = "name";
    }

}