package br.com.corunda.fastandfurious.service;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import br.com.corunda.fastandfurious.MainActivity;
import br.com.corunda.fastandfurious.R;
import br.com.corunda.fastandfurious.listener.FastLocationListener;

public class FuriousService extends Service {

    public static final String TAG = "FuriousService";
    public static final String CHANNEL_ID = "FuriousServiceChannel";

    private FastLocationListener listener;
    private LocationManager locationManager;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.v(TAG, "onBind");
        return null;
    }

    @Override
    public void onCreate() {
        Log.v(TAG, "onCreate");

        listener = new FastLocationListener(this);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if(locationManager != null) {
            boolean permissionAccessCoarseLocationApproved = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
            if (permissionAccessCoarseLocationApproved) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, listener);
            } else {
                Log.v(TAG, "permissao necessaria");
            }
        }

        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.v(TAG, "onStartCommand");

        NotificationChannel serviceChannel = new NotificationChannel(CHANNEL_ID, "Foreground Service Channel", NotificationManager.IMPORTANCE_DEFAULT);
        NotificationManager manager = getSystemService(NotificationManager.class);
        manager.createNotificationChannel(serviceChannel);

        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Notification notification =
                new Notification.Builder(this, CHANNEL_ID)
                        .setContentTitle(getText(R.string.app_name))
                        .setContentText(getText(R.string.running_txt_sub_item_name_tooltip))
                        .setSmallIcon(R.drawable.ic_launcher_bike_foreground)
                        .setContentIntent(pendingIntent)
                        .setTicker(getText(R.string.app_name))
                        .build();

        startForeground(1, notification);
        Log.v(TAG, "startForegroundService");

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.v(TAG, "onDestroy");

        if (listener != null) {
            listener.stop();
            if (locationManager != null) {
                locationManager.removeUpdates(listener);
            }
        }

        super.onDestroy();
    }
}
