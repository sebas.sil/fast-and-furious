package br.com.corunda.fastandfurious;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import br.com.corunda.fastandfurious.adapter.RunningCursorAdapter;
import br.com.corunda.fastandfurious.database.FastContract;
import br.com.corunda.fastandfurious.database.FuriousProvider;

public class RunningActivity extends AppCompatActivity {

    public static final int EDIT_CONTACT_REQUEST = 1;
    public static final String TAG = "RunningActivity";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == EDIT_CONTACT_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                Log.v(TAG, "Retornou com sucesso");

                RecyclerView recyclerView = this.findViewById(R.id.running_recycler_view);
                recyclerView.getAdapter().notifyDataSetChanged();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.v(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_runnings);

        RecyclerView recyclerView = this.findViewById(R.id.running_recycler_view);
        View emptyView = this.findViewById(R.id.empty_view);

        Cursor cursor = getContentResolver().query(FuriousProvider.CONTENT_RUNNING_URI, new String[]{"R." + FastContract.RunningEntry._ID, "R." + FastContract.RunningEntry.COLUMN_NAME_NAME, "SUM(P." + FastContract.PointEntry.COLUMN_NAME_DISTANCE+") distance", "max(P."+FastContract.PointEntry.COLUMN_NAME_TIME+") - min(P."+FastContract.PointEntry.COLUMN_NAME_TIME+") time", "avg(P."+FastContract.PointEntry.COLUMN_NAME_SPEED+") velocity", "(SELECT sum(f.altitude - n.altitude) elev FROM ff f INNER JOIN ff n ON f._id = n._id + 1 AND f.running_id = n.running_id AND f.running_id = R._id AND (f.altitude - n.altitude) > 0) altitude"}, null, null, null);

        if (cursor.getCount() == 0) {
            recyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            RunningCursorAdapter todoAdapter = new RunningCursorAdapter(cursor);
            recyclerView.setAdapter(todoAdapter);
        }
    }
}
