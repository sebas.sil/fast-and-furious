package br.com.corunda.fastandfurious;

import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import br.com.corunda.fastandfurious.database.FastContract;
import br.com.corunda.fastandfurious.database.FuriousProvider;

public class EditRunningActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_running);

        long id = (Long) getIntent().getSerializableExtra("running");
        Uri singleRun = ContentUris.withAppendedId(FuriousProvider.CONTENT_RUNNING_URI, id);

        Cursor cursor = getContentResolver().query(singleRun, null, null, null, null);

        if (cursor.moveToNext()) {
            EditText edit = findViewById(R.id.edit_running_name);
            edit.setText(cursor.getString(cursor.getColumnIndexOrThrow(FastContract.RunningEntry.COLUMN_NAME_NAME)));
        }
    }

    public void save(View view) {
        EditText edit = findViewById(R.id.edit_running_name);
        if (edit.getText().length() > 0) {
            long id = (Long) getIntent().getSerializableExtra("running");
            Uri singleUri = ContentUris.withAppendedId(FuriousProvider.CONTENT_RUNNING_URI, id);

            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(FastContract.RunningEntry.COLUMN_NAME_NAME, edit.getText().toString());

            this.getContentResolver().update(singleUri, values, BaseColumns._ID + "=?", new String[]{String.valueOf(id)});
            this.getContentResolver().notifyChange(FuriousProvider.CONTENT_RUNNING_URI, null);
            this.setResult(Activity.RESULT_OK);
            this.finish();
        }
    }

}
