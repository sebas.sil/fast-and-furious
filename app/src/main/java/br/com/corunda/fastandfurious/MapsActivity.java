package br.com.corunda.fastandfurious;

import android.content.ContentUris;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import br.com.corunda.fastandfurious.database.FastContract;
import br.com.corunda.fastandfurious.database.FastReaderDbHelper;
import br.com.corunda.fastandfurious.database.FuriousProvider;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    FastReaderDbHelper dbHelper;
    private long id = -1;

    public static final String TAG = "MapsActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        if(mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
        dbHelper = new FastReaderDbHelper(this);
        id = (Long) getIntent().getSerializableExtra("running");

        Log.v(TAG, "running id: " + id);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        Uri singleRun = ContentUris.withAppendedId(FuriousProvider.CONTENT_RUNNING_POINTS_URI, id);

        Cursor cursor = getContentResolver().query(singleRun, null, null, null, null);


        MarkerOptions markerOptions = null;
        PolylineOptions lines = new PolylineOptions();
        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
        while(cursor.moveToNext()) {
            float lt = cursor.getFloat(cursor.getColumnIndexOrThrow(FastContract.PointEntry.COLUMN_NAME_LATITUDE));
            float lg = cursor.getFloat(cursor.getColumnIndexOrThrow(FastContract.PointEntry.COLUMN_NAME_LONGITUDE));
            float sp = cursor.getFloat(cursor.getColumnIndexOrThrow(FastContract.PointEntry.COLUMN_NAME_SPEED));
            float a = cursor.getFloat(cursor.getColumnIndexOrThrow(FastContract.PointEntry.COLUMN_NAME_ACCELERATION_Z));
            float acc = cursor.getFloat(cursor.getColumnIndexOrThrow(FastContract.PointEntry.COLUMN_NAME_ACCURACY));
            //markerOptions = new MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(iconGen.makeIcon(sp + " km/h ("+a+")"))).position(new LatLng(lt, lg)).snippet(sp + " km/h ("+a+")");
            markerOptions = new MarkerOptions().position(new LatLng(lt, lg)).title(String.format("%.2f km/h", sp)).snippet(String.format("resultante: %.2fm/s", a)).alpha(0f);
            googleMap.addMarker(markerOptions);
            googleMap.addCircle(new CircleOptions()
                    .center(new LatLng(lt, lg))
                    .radius(acc)
                    .fillColor(Color.argb(128, 0, 0, 255)))
                    .setStrokeWidth(0f);
            lines.add(new LatLng(lt, lg));
            boundsBuilder.include(new LatLng(lt, lg));
        }
        cursor.close();
        lines.width(10);
        lines.color(Color.BLUE);
        googleMap.addPolyline(lines);
        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney").alpha(1f).flat(true).snippet("teste"));
        Log.v(TAG, "marker: " + markerOptions);
        if(markerOptions != null) {
            int routePadding = 100;
            LatLngBounds latLngBounds = boundsBuilder.build();
            Log.v(TAG, "latLngBounds: " + latLngBounds);
            try {
                //FIXME Map size can't be 0. Most likely, layout has not yet occured for the map view. provavelmente quando tem apenas um ponto
                googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, routePadding));
            } catch(Exception e){
                Log.e(TAG, "Map size can't be 0");
                LatLngBounds route = new LatLngBounds(lines.getPoints().get(0), lines.getPoints().get(lines.getPoints().size()-1));
                Log.v(TAG, "map point: " + route);
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.getCenter(), 5));
            }
        }

        Log.v(TAG, "total points: " + lines.getPoints().size());
    }
}
