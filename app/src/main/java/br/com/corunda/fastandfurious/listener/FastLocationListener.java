package br.com.corunda.fastandfurious.listener;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

import br.com.corunda.fastandfurious.MainActivity;
import br.com.corunda.fastandfurious.database.FastContract;
import br.com.corunda.fastandfurious.database.FastReaderDbHelper;

public class FastLocationListener implements LocationListener, Serializable {

    private static final String TAG = "FastLocationListener";

    private final Context context;
    private Location lastLocation;
    private FastReaderDbHelper dbHelper;
    private long runningId = 0;

    private AccelerometerListener accelerometerListener;
    private SensorManager sensorManager;
    private Sensor accelerometer;
    private Intent intent = new Intent(MainActivity.REQUEST_LOCATION_UPDATE);

    public FastLocationListener(Context context) {
        this.context = context;
        dbHelper = new FastReaderDbHelper(context);

        accelerometerListener = new AccelerometerListener(context);
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        // ignores the gravity
        if (sensorManager != null) {
            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
            sensorManager.registerListener(accelerometerListener, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        if (this.lastLocation == null) {
            this.lastLocation = location;
        }
        // pega a aceleracao no eixo Z (as costas do celular)
        float[] acceleration = accelerometerListener.getSensor() != null ? accelerometerListener.getSensor().values : new float[]{0, 0, 0};
        //String text = String.format("(x=%.3f,y=%.3f) | v=%.2f d=%.2f) | z=%.2f", location.getLatitude(), location.getLongitude(), location.getSpeed() * 3.6, location.distanceTo(lastLocation), acceleration[2]);
        //Log.v(TAG, text);
        //Toast.makeText(context, text, Toast.LENGTH_SHORT).show();

        // send value to the receiver
        intent.putExtra("location", location);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        // Gets the data repository in write mode
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        if (runningId == 0) {
            String cityName = null;
            try {
                // get the city name
                Geocoder gcd = new Geocoder(context, Locale.getDefault());
                List<Address> addresses = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                if (addresses.size() > 0) {
                    cityName = String.format("%s, %s", addresses.get(0).getSubLocality(), addresses.get(0).getSubAdminArea());
                } else {
                    cityName = String.format("(%f, %f)", location.getLatitude(), location.getLongitude());
                }
            } catch (Exception e) {
                Log.e(TAG, "Erro ao capturar localização da corrida", e);
            }

            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(FastContract.RunningEntry.COLUMN_NAME_NAME, cityName);
            // Insert the new row, returning the primary key value of the new row
            runningId = db.insert(FastContract.RunningEntry.TABLE_NAME, null, values);
        }

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(FastContract.PointEntry.COLUMN_NAME_RUNNING, runningId);
        values.put(FastContract.PointEntry.COLUMN_NAME_LATITUDE, location.getLatitude());
        values.put(FastContract.PointEntry.COLUMN_NAME_LONGITUDE, location.getLongitude());
        values.put(FastContract.PointEntry.COLUMN_NAME_ALTITUDE, location.getAltitude());
        values.put(FastContract.PointEntry.COLUMN_NAME_ACCURACY, location.getAccuracy());
        values.put(FastContract.PointEntry.COLUMN_NAME_SPEED, location.getSpeed() * 3.6); // transform from m/s to km/h
        values.put(FastContract.PointEntry.COLUMN_NAME_SPEED_ACCURACY, location.getSpeedAccuracyMetersPerSecond() * 3.6); // transform from m/s to km/h
        values.put(FastContract.PointEntry.COLUMN_NAME_TIME, location.getTime());
        values.put(FastContract.PointEntry.COLUMN_NAME_DISTANCE, location.distanceTo(lastLocation));
        values.put(FastContract.PointEntry.COLUMN_NAME_ACCELERATION_X, acceleration[0]);
        values.put(FastContract.PointEntry.COLUMN_NAME_ACCELERATION_Y, acceleration[1]);
        values.put(FastContract.PointEntry.COLUMN_NAME_ACCELERATION_Z, acceleration[2]);

        // Insert the new row, returning the primary key value of the new row
        db.insert(FastContract.PointEntry.TABLE_NAME, null, values);
        this.lastLocation = location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.v(TAG, "onStatusChanged: " + provider);
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.v(TAG, "onProviderEnabled: " + provider);
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.v(TAG, "onProviderDisabled: " + provider);
    }

    public void stop() {
        Log.v(TAG, "stop");
        try {

            // Define a projection that specifies which columns from the database you will actually use after this query.
            String[] projection = {
                    BaseColumns._ID,
                    FastContract.RunningEntry.COLUMN_NAME_NAME
            };

            // Gets the data repository in write mode
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            Cursor cursor = db.query(
                    FastContract.RunningEntry.TABLE_NAME,     // The table to query
                    projection,                             // The array of columns to return (pass null to get all)
                    FastContract.RunningEntry._ID + "=?", // The columns for the WHERE clause
                    new String[]{String.valueOf(runningId)},                        // The values for the WHERE clause
                    null,                           // don't group the rows
                    null,                            // don't filter by row groups
                    null                            // The sort order
            );


            if (cursor.moveToNext()) {
                String endCityName = null;
                String startCityName = cursor.getString(cursor.getColumnIndexOrThrow(FastContract.RunningEntry.COLUMN_NAME_NAME));
                try {
                    // get the city name
                    Geocoder gcd = new Geocoder(context, Locale.getDefault());
                    List<Address> addresses = gcd.getFromLocation(this.lastLocation.getLatitude(), this.lastLocation.getLongitude(), 1);
                    if (addresses.size() > 0) {
                        endCityName = String.format("%s, %s", addresses.get(0).getSubLocality(), addresses.get(0).getSubAdminArea());
                    } else {
                        endCityName = String.format("(%f, %f)", this.lastLocation.getLatitude(), this.lastLocation.getLongitude());
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Erro ao capturar localização da corrida", e);
                }

                String cityName = startCityName + " - " + endCityName;

                // Create a new map of values, where column names are the keys
                ContentValues values = new ContentValues();
                values.put(FastContract.RunningEntry.COLUMN_NAME_NAME, cityName);
                // Insert the new row, returning the primary key value of the new row
                runningId = db.update(FastContract.RunningEntry.TABLE_NAME, values, "_ID=?", new String[]{String.valueOf(runningId)});
            }

        } finally {
            dbHelper.close();
            sensorManager.unregisterListener(accelerometerListener);
        }
    }
}
