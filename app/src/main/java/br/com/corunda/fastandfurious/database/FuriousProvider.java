package br.com.corunda.fastandfurious.database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Arrays;

public class FuriousProvider extends ContentProvider {

    public static final String PROVIDER_NAME = FuriousProvider.class.getName();
    private static final String URL = "content://" + PROVIDER_NAME + "/runnings";
    public static final Uri CONTENT_RUNNING_URI = Uri.parse(URL);
    public static final Uri CONTENT_RUNNING_POINTS_URI = Uri.parse(URL + "/points");
    public static final String TAG = "FuriousProvider";

    private static final int RUNNINGS = 1;
    private static final int RUNNING_ID = 2;
    private static final int POINTS = 3;

    private SQLiteDatabase db;

    static final UriMatcher uriMatcher;
    static{
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "runnings", RUNNINGS);
        uriMatcher.addURI(PROVIDER_NAME, "runnings/#", RUNNING_ID);
        uriMatcher.addURI(PROVIDER_NAME, "runnings/points/#", POINTS);
    }

    @Override
    public boolean onCreate() {
        Context context = getContext();
        FastReaderDbHelper dbHelper = new FastReaderDbHelper(context);
        db = dbHelper.getWritableDatabase();
        return (db == null)? false:true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        Log.v(TAG, "query: " + uri + ", projection: " + Arrays.toString(projection) + ", selection: " + selection + ", selectionArgs: " + Arrays.toString(selectionArgs));
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        switch (uriMatcher.match(uri)) {
            case RUNNING_ID:
                qb.setTables(FastContract.RunningEntry.TABLE_NAME);
                qb.appendWhere(BaseColumns._ID + "=" + uri.getPathSegments().get(1));
                break;
            case RUNNINGS:
                sortOrder = "R." + BaseColumns._ID + " DESC";
                if(selection == null){
                    selection = " 1=1) GROUP BY (R." + FastContract.RunningEntry._ID;
                }
                qb.setTables(FastContract.RunningEntry.TABLE_NAME + " R INNER JOIN " + FastContract.PointEntry.TABLE_NAME + " P ON P." + FastContract.PointEntry.COLUMN_NAME_RUNNING + " = R." + FastContract.RunningEntry._ID);
                qb.setProjectionMap(null);
                break;
            case POINTS:
                selection = null;
                // change table name
                qb.setTables(FastContract.PointEntry.TABLE_NAME + " P ");
                // change where to point to the running id
                qb.appendWhere("P." + FastContract.PointEntry.COLUMN_NAME_RUNNING + "=" + uri.getPathSegments().get(2));
                sortOrder = "P." + BaseColumns._ID + " DESC";
                break;
            default:
        }

        Cursor c = qb.query(db,	projection,	selection, selectionArgs,null, null, sortOrder);

        /**
         * register to watch a content URI for changes
         */
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)){
            /**
             * Get all running records
             */
            case RUNNINGS:
                return "vnd.android.cursor.dir/" + PROVIDER_NAME;
            /**
             * Get a particular running
             */
            case RUNNING_ID:
                return "vnd.android.cursor.item/" + PROVIDER_NAME;
            /**
             * Get all running points records
             */
            case POINTS:
                return "vnd.android.cursor.dir/" + PROVIDER_NAME;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        Log.v(TAG, "insert: " + uri);
        /**
         * Add a new running record
         */
        long rowID = db.insert(FastContract.RunningEntry.TABLE_NAME, null, values);

        /**
         * If record is added successfully
         */
        if (rowID > 0) {
            Uri newUri = ContentUris.withAppendedId(CONTENT_RUNNING_URI, rowID);
            getContext().getContentResolver().notifyChange(newUri, null);
            getContext().getContentResolver().notifyChange(CONTENT_RUNNING_URI, null);
            return newUri;
        }

        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        Log.v(TAG, "delete: " + uri);
        int count;
        switch (uriMatcher.match(uri)){
//            case RUNNINGS:
//                count = db.delete(FastContract.RunningEntry.TABLE_NAME, selection, selectionArgs);
//                break;

            case RUNNING_ID:
                String id = uri.getPathSegments().get(1);
                // Just delete by ID
                db.delete( FastContract.PointEntry.TABLE_NAME, FastContract.PointEntry.COLUMN_NAME_RUNNING +  "=?", new String[]{id});
                count = db.delete( FastContract.RunningEntry.TABLE_NAME, BaseColumns._ID +  "=?", new String[]{id});
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        getContext().getContentResolver().notifyChange(CONTENT_RUNNING_URI, null);
        return count;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        Log.v(TAG, "update: " + uri);
        int count;
        switch (uriMatcher.match(uri)) {
            case RUNNINGS:
                count = db.update(FastContract.RunningEntry.TABLE_NAME, values, selection, selectionArgs);
                break;

            case RUNNING_ID:
                count = db.update(FastContract.RunningEntry.TABLE_NAME, values,BaseColumns._ID + "=?", new String[]{uri.getPathSegments().get(1)});
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri );
        }

        getContext().getContentResolver().notifyChange(uri, null);
        getContext().getContentResolver().notifyChange(CONTENT_RUNNING_URI, null);
        return count;
    }
}
